import ReactDOM from 'react-dom';
import React from 'react';

const el = document.getElementById('root');

// exemplo 04.1
//  ReactDOM.render(
//     <div>
//       <strong>
//         Olá React!
//       </strong>
//     </div>,
//     el
//  );

// exemplo 04.2
const tag = <strong>Olá React!</strong>;

ReactDOM.render(
  <div>
    { tag }
  </div>,
  el
);