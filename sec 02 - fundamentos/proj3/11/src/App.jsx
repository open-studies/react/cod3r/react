import React from 'react';

// export default function App(props) {
// export default function(props) {

// da seguinte forma, teria que importar no index desestruturando:
// import { App } from './App';
// export daqui:
// export function App(props) {
// export default (props) => {
// export default props => {
// export default () => {
// underline para indicar que há 1 parametro, mas nao será usado
// export default _ => {

// sem as chaves delimitadoras de escopo/corpo da função, está sendo retornado de forma implícita tudo que está no corpo da função (e por isso, fica sem a instrução de return que seria repetitivo)
// export default _ =>
const App = _ => (
  <div>
    <h1>Fundamentos React</h1>
  </div>
);

export default App;
