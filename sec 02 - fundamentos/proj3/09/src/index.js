import './index.css';
import ReactDOM from 'react-dom';
import React from 'react';

import ComParametro from './components/basicos/ComParametro';

const el = document.getElementById('root');

ReactDOM.render(
  <div>
    <ComParametro
      titulo="Situação do Aluno"
      aluno="João"
      notaNum={6.9}
    />
  </div>,
  el
);