import './index.css';
import ReactDOM from 'react-dom';
import React from 'react';

import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';
import Fragmento from './components/basicos/Fragmento';

const el = document.getElementById('root');

ReactDOM.render(
  // renderizar 2 irmãos causará erro
  // <Primeiro/>
  // <ComParametro
  //   titulo="Situação do Aluno"
  //   aluno="João"
  //   notaNum={6.9}
  // />
  <div>
    <Primeiro/>
    <ComParametro
      titulo="Situação do Aluno"
      aluno="João"
      notaNum={6.9}
    />
    <Fragmento/>
  </div>,
  el
);