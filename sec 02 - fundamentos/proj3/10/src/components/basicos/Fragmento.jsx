import React from "react";

export default function Fragmento() {
  return (
    // Formas de usar Fragmento:

    // a partir apenas do React já importado
    // <React.Fragment></React.Fragment>

    // importando separadamente
    // <Fragment></Fragment>

    // de modo curto.
    // impede uso de atributo
    // <></>
    <>
      <h2>Fragmento</h2>
      <p>Cuidado com esse erro!</p>
    </>
  );
}