import React from 'react'

// export default function ComParametro(params) {
//   return (
//     <div>
//       <h2>{ params.titulo }</h2>
//       <p>{ params.subtitulo }</p>
//     </div>
//   );
// }

// export default function ComParametro(props) {
//   return (
//     <div>
//       <h2>{ props.titulo }</h2>
//       <p>{ props.subtitulo }</p>
//     </div>
//   );
// }

export default function ComParametro(props) {
  const status = props.notaNum >= 7 ? 'Aprovado(a)' : "Em recuperação";
  return (
    <div>
      <h2>{ props.titulo }</h2>
      <p>
        <strong>{ props.aluno }</strong> tem nota { props.notaStr } ({ props.notaNum }) e está { status }.
      </p>
    </div>
  );
}
