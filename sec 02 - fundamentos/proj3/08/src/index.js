import './index.css';
import ReactDOM from 'react-dom';
import React from 'react';

import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';

const el = document.getElementById('root');

ReactDOM.render(
  <div>
    <Primeiro></Primeiro>
    {/* <ComParametro
      titulo="2o componente"
      subtitulo="Legal" /> */}
    <ComParametro
      titulo="Situação do Aluno"
      aluno="João"
      notaStr="9.8"
      notaNum={6.9}
    />
    <ComParametro
      titulo="Situação do Aluno"
      aluno="Maria"
      notaStr="10"
      notaNum={10}
    />
  </div>,
  el
);