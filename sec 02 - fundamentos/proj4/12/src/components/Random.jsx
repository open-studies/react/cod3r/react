import React from 'react';

const Random = props => {
  const max = props.max > props.min ? props.max : props.min;
  const min = props.min < props.max ? props.min : props.max;
  const random = (max - min) * Math.random() + min;
  return (
    <>
      <h1>Número aleatório</h1>
      <p>
        <strong>Número sorteado:</strong> {random}
      </p>
    </>
  );
};
export default Random;
