import React from 'react';
import Random from './components/Random';

const App = () => (
  <div>
    <h1>Desafio</h1>
    <Random min={2} max={9} />
  </div>
);

export default App;
