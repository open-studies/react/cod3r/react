import React from 'react';

// export default function Primeiro() {
//  return <h2>Primeiro Componente JSX</h2>
// }

// export default function Primeiro() {
//   return <div>
//     <h2>Primeiro Componente JSX com div</h2>
//   </div>
// }

export default function Primeiro() {
  const msg = 'Seja bem vindo(a)!';
  return (
    <div>
      <h2>Primeiro Componente JSX com div em expressão</h2>
      <p>{ msg }</p>
    </div>
  );
}