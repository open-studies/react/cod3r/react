# React - S02.01 Notes

> - "Biblioteca com cara de FW."
> - "Uma das mais usadas pra construção de apps FE tanto web quanto mobile."
> - "O foco aqui será web."
> - "Mas os princípios podem ser aplicados para apps mobile também, com o uso do [RN](https://reactnative.dev/), caso necessite."
> - "RN não será abordado".

> No geral, será visto:
> - Fundamentos de React (nesse capítulo)
> - Desenvolvimento de aplicações-desafio
> - Hooks
> - Gerenciamento de estado
> - Redux
> - React atual
