# React - S02.04 Notes

## JSX

Para o ReactDOM interpretar elementos `html` no interior do método de renderização, usa-se a sintaxe JSX.

JSX ("JS XML") é uma extensão da sintaxe do JS semelhante ao XML. Criada pelo Facebook, permite mesclar no JS tanto marcação (estrutura) como lógica, durante o processo de construção de componentes no React.

Sua sintaxe tem aparência assim:
```js
const el = <h1 className="saudacao">Olá!</h1>;
```
Note que:
- a sintaxe semalhante ao XML (ou HTML, dependendo do conteúdo) não é escrita usando aspas duplas (`"`) ou simples (`'`) ou mesmo _backticks_ (`` ` ``), como seria uma expressão JS que não sejam tipos de dados primitidos em JS. O conteúdo não é string, e sim componente React.
- o que seria o atributo `class` em `HTML` no `JSX` passa a ser `className`, pois no `JS` (já que JSX é JS pre-processado) já existe a palavra reservada `class`. Vide [thread](https://stackoverflow.com/a/46991278) e [doc](https://reactjs.org/docs/glossary.html#jsx:~:text=The%20attribute%20class%20is%20also%20written%20as%20className%20since%20class%20is%20a%20reserved%20word%20in%20JavaScript%3A).
- Atributos de componentes em JSX usam convenção de nomenclatura [camelCase](https://en.wikipedia.org/wiki/Naming_convention_(programming)#Examples_of_multiple-word_identifier_formats) pra demonstrar que se aproxima mais do JS do que do HTML (que usa atributos de nomes compostos com tudo minúsculo e junto). Vide [doc](https://reactjs.org/docs/introducing-jsx.html#specifying-attributes-with-jsx:~:text=Since%20JSX%20is%20closer%20to%20JavaScript,convention%20instead%20of%20HTML%20attribute%20names.).

Embora não obrigatório para construir componentes no React (que podem ser escritos em JS puro), facilita o processo de criação.

Permite o uso interno de expressões JS com o uso de `{ expression }`:
```js
const el = <h1 className="saudacao">Olá, { user }!</h1>;
```

Sem usar as `{ }`, as expressões JS não são resolvidas como tal e passam a serem interpretadas apenas como string.


## Contexto de uso
A intenção dele é de funcionar em contexto de pre-processamento (transpilamento) e não diretamente em browsers.

Ela funciona como uma linguagem de pré-processamento que é transpilada para JS e não para XML ou HTML, apesar da semelhança.

Para o transpilamento de JSX para JS (o método do React, em sintaxe JS pura), é necessário adicionar o Babel como dependência do projeto (da aplicação que usa React como dependência), além de ter o React adicionado, pois ele é o compilador responsável pelo transpilamento de linguagens e gerações recentes de JS para outras gerações de JS.

Mesmo que o Babel seja uma dependência de desenvolvimento do React, por não estar na versão que é distribuição no React (ambiente de produção do React), ele deve ser usado pelas aplicações que usam React.

MAS, se o projeto for criado com o uso do CRA, a inclusão e configuração do Babel (e outras dependências) já é providenciada.

## Transpilamento
Mais especificamente, JSX é transpilada para método JS (`.createElement()`) de uma necessária instância do React (deve haver uma instância importada do React no escopo) que cria um componente React no Virtual DOM, com os devidos atributos e filhos, conforme o componente for implementado no JSX. Esse método para o qual o JSX é transpilado faz parte da especificação da API do React cuja sintaxe (vide [doc](https://reactjs.org/docs/react-api.html#createelement)) é:
```js
React.createElement(
  type,
  [props],
  [...children]
)
```

Esse método do React então, após as devidas verificações internas, retorna um objeto JS puro, o qual descreve os aspectos de um elemento HTML. É a estrutura de um componente React em formato de objeto JS.

Chamando o método do React em uma constante de nome `element` o resultado gerado tem essa sintaxe:
```js
const element = {
  type: '',
  props: {
    className: '',
    children: ''
  }
};
```

## Fases de um JSX
Como exemplo, segue um trecho de código em JSX e suas subsequentes etapas de processamento. Exemplo semelhante ao usado na [doc do React](https://reactjs.org/docs/introducing-jsx.html#jsx-represents-objects).

Aqui é o código é armazenado em uma constante chamada `el`, mas poderia ser qualquer outro nome (ou modalidade de variável).

Como JSX:
```js
const el = (
  <h1 className="saudacao">
    Olá!
  </h1>
);
``` 

Após transpilamento:
```js
const el = React.createElement(
  'h1',
  {className: 'saudacao'},
  'Olá!'
);
```

Após retorno do método do React:
```js
const el = {
  type: 'h1',
  props: {
    className: 'saudacao',
    children: 'Olá!'
  }
};
```

## Requisito
Para funcionar no projeto:
- o React deve estar importado no escopo do módulo em que o `JSX` for usado;
- o Babel deve estar importado e configurado no projeto (a menos que a app tenha sido criada usando `CRA` ou outro facilitador de criação e configuração de aplicações em React).