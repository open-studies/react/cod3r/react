import React from 'react'

export default function ComParametro(props) {
  const status = props.notaNum >= 7 ? 'Aprovado(a)' : "Em recuperação";

  const nota = Math.ceil(props.notaNum);
  
  return (
    <div>
      <h2>{ props.titulo }</h2>
      <p>
        <strong>{ props.aluno }</strong> tem nota { nota } e está { status }.
      </p>
    </div>
  );
}
