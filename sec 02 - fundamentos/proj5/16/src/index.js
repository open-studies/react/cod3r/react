import './index.css';
import ReactDOM from 'react-dom';
import React from 'react';
import "@fontsource/oswald/latin.css"
import App from './App';

const el = document.getElementById('root');

ReactDOM.render(
  <App/>,
  el
);