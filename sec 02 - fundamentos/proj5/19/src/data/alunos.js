const alunos = [
  { id: 1, nome: 'Ana', nota: 9.2 },
  { id: 2, nome: 'Bia', nota: 9.1 },
  { id: 3, nome: 'Charles', nota: 9.0 },
  { id: 4, nome: 'Diana', nota: 8.9 },
  { id: 5, nome: 'Ellen', nota: 8.8 },
  { id: 6, nome: 'Fátima', nota: 8.7 },
  { id: 7, nome: 'George', nota: 8.6 },
  { id: 8, nome: 'Hillary', nota: 8.5 },
  { id: 9, nome: 'Igor', nota: 8.4 },
];

export default alunos;
