import React from 'react';

const Random = props => {
  const {min: _min, max: _max} = props;
  const max = _max > _min ? _max : _min;
  const min = _min < _max ? _min : _max;
  const random = (max - min) * Math.random() + min;
  return (
    <>
      <h1>Número aleatório</h1>
      <p>
        <strong>Valor mínimo:</strong> {min}
      </p>
      <p>
        <strong>Valor máximo:</strong> {max}
      </p>
      <p>
        <strong>Valor sorteado:</strong> {random}
      </p>
    </>
  );
};
export default Random;
