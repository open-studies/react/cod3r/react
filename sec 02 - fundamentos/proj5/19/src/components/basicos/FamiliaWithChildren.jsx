import React, { cloneElement, Children } from 'react';

const FamiliaWithChildren = props => {
  const style = {
    color: '#888',
  };
  return (
    <div>
      <p style={style}>Map passando props do pai:</p>
      {Children.map(props.children, (child, i) => cloneElement(child, { ...props, key: i }))}
      <p style={style}>Props.children:</p>
      {props.children}
    </div>
  );
};

export default FamiliaWithChildren;
