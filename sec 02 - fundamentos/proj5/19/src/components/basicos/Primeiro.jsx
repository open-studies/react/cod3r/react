import React from 'react';

export default function Primeiro() {
  const msg = 'Componente em arquivo .jsx';
  return (
    <div>
      <h2>Primeiro Componente</h2>
      <p>{ msg }</p>
    </div>
  );
}