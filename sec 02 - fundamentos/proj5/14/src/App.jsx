import React from 'react';
import Card from './components/layout/Card';
import Random from './components/basicos/Random';
import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';
import Fragmento from './components/basicos/Fragmento';


const App = () => (
  <div>
    <h1>Fundamentos React</h1>

    <Card titulo="#04 - Desafio Aleatório">
      <Random min={2} max={9} />
    </Card>

    <Card titulo="#03 - Fragmento">
      <Fragmento/>
    </Card>

    <Card titulo="#02 - Com Parâmetro">
      <ComParametro
        titulo="Situação do Aluno"
        aluno="João"
        notaNum={6.9}
      />
    </Card>

    <Card titulo="#01 - Primeiro Componente">
      <Primeiro/>
    </Card>

  </div>
);

export default App;
