import React from 'react';
import Card from './components/layout/Card';
import Random from './components/basicos/Random';
import Primeiro from './components/basicos/Primeiro';
import ComParametro from './components/basicos/ComParametro';
import Fragmento from './components/basicos/Fragmento';
import './App.css';
import Familia from './components/basicos/Familia';

const App = () => (
  <div className="App">
    <h1>Fundamentos React</h1>
    <div className="Cards">
      <Card titulo="#05 - Componente com Filho" color="#00c8f9">
        <Familia sobrenome="Ferreira"/>
      </Card>
      
      <Card titulo="#04 - Desafio Aleatório" color="#fa6900">
        <Random min={2} max={9} />
      </Card>

      <Card titulo="#03 - Fragmento" color="#e94c6f">
        <Fragmento />
      </Card>

      <Card titulo="#02 - Com Parâmetro" color="#e8b71a">
        <ComParametro titulo="Situação do Aluno" aluno="João" notaNum={6.9} />
      </Card>

      <Card titulo="#01 - Primeiro Componente" color="#588c73">
        <Primeiro />
      </Card>
    </div>
  </div>
);

export default App;
