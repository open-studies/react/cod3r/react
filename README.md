# React

## TOC
- [sec 01 - dev env setup](sec%2001%20-%20dev%20env%20setup/README.md)
- [sec 02 - fundamentos](sec%2002%20-%20fundamentos/README.md)

## Anotações de aula

- **Curso**: 'React'
- **Instrutor**: [Leonardo Moura Leitão](https://github.com/leonardomleitao) (['Cod3r Cursos'](https://www.cod3r.com.br/))

## Apresentação do repositório
Esse repositório é o registro de meus estudos sobre React assistindo o [curso da Cod3r na Udemy](https://www.udemy.com/course/react-redux-pt/), mas também lendo diversos artigos pela internet e documentações nos sites das tecnologias envolvidas.

## Organização
A estrutura do repositório segue:
- no 1o nível de diretórios (na root), a grade de conteúdos na qual o curso é organizado (seções)
- no 2o nível de diretórios, com anotação de introdução da seção e os diretórios:
  - de todas as anotações da seção
  - dos projetos, sendo mais de um diretório de projeto, quando a seção assim o fizer ou tiver uma quebra da ideia que o código exemplificado tava seguindo

Quando for o caso desses projetos conseguirem ser independentes do contexto desse repositório, usarei submódulo, para linkar esses diretórios com outros (no GitHub), cujo rastreamento de versões passa a ser independente deste principal.

## Autoria das anotações

Todas as anotações feitas nos arquivos de `markdown` (`.md`) são resultado de:
- interpretações e sínteses do que foi dito pelo instrutor
- resultado de leitura, síntese e interpretação de:
  - tutoriais e guias diversos lidos pela internet
  - documentações das respectivas tecnologias
- familiaridade de alguns tópicos que eu já tinha

Em todos os casos em que:
- me baseei em alguma fonte externa, incluí os links no texto.
- a expressão escrita está entre aspas ou marcada como citação (via sintaxe do `.md` pra isso), foi citação direta ou indireta (nesse caso, indico que adaptei) do instrutor.
- não há links nem aspas, foi texto elaborado por mim.
